sheepdog (0.8.3-6) UNRELEASED; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use debhelper-compat instead of debian/compat.

 -- Ondřej Nový <onovy@debian.org>  Mon, 12 Feb 2018 10:36:18 +0100

sheepdog (0.8.3-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ David Rabel ]
  * Add pt_BR debconf translation (Closes: #827326)
  * Add patch of Chris Lamb <lamby@debian.org> to make build reproducible
    (Closes: #835051).

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Jan 2018 23:28:14 +0100

sheepdog (0.8.3-4.1) unstable; urgency=medium

  [ Arturo Borrero Gonzalez ]
  * Non-maintainer upload.
  * d/control: update build-deps on corosync, libcorosync-dev no longer
    exists.

 -- Christoph Berg <christoph.berg@credativ.de>  Fri, 15 Jan 2016 13:33:54 +0100

sheepdog (0.8.3-4) unstable; urgency=medium

  * Refreshed patches.
  * Added patch for the script that generates the bash_completion
    file to produce stable output in order to make builds reproducible.
    Thanks to Reiner Herrmann <reiner@reiner-h.de> for the bug report and
    patch (Closes: #775229).

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Jan 2015 15:25:09 +0000

sheepdog (0.8.3-3) unstable; urgency=medium

  * Initial Dutch translation of debconf messages thanks to Frans
    Spiesschaert <Frans.Spiesschaert@yucom.be> (Closes: #767245).

 -- Thomas Goirand <zigo@debian.org>  Thu, 08 Jan 2015 14:25:49 +0000

sheepdog (0.8.3-2) unstable; urgency=medium

  * Initial Dutch translation of debconf messages thanks to Frans
    Spiesschaert <Frans.Spiesschaert@yucom.be> (Closes: #767245).

 -- Thomas Goirand <zigo@debian.org>  Wed, 07 Jan 2015 16:07:48 +0000

sheepdog (0.8.3-1) unstable; urgency=medium

  * New upstream release 0.8.3.
  * Add --upgrade options to start sheep(s).

 -- YunQiang Su <wzssyqa@gmail.com>  Mon, 25 Aug 2014 15:30:51 +0800

sheepdog (0.8.1-4) unstable; urgency=medium

  * Use KILL to stop daemon.

 -- YunQiang Su <wzssyqa@gmail.com>  Sun, 18 May 2014 23:19:28 +0800

sheepdog (0.8.1-3) unstable; urgency=medium

  * Enable http simple storage support.

 -- YunQiang Su <wzssyqa@gmail.com>  Wed, 30 Apr 2014 10:36:44 +0800

sheepdog (0.8.1-2) unstable; urgency=medium

  * Fix bash completion (Closes: #745108).

 -- YunQiang Su <wzssyqa@gmail.com>  Mon, 28 Apr 2014 21:49:10 +0800

sheepdog (0.8.1-1) unstable; urgency=medium

  * Imported Upstream version 0.8.1
  * Fix gitweb URL.
  * Refreshed patches

 -- YunQiang Su <wzssyqa@gmail.com>  Sun, 23 Mar 2014 02:07:06 +0800

sheepdog (0.7.5-1) unstable; urgency=low

  * New upstream release 0.7.5: Remove patch
    Makefile-am-CPPFLAGS-instead-INCLUDES, merged to upstream
  * Use subdir-objects AC option to remove automake warning.
  * Define EFD_SEMAPHORE if not exists.
  * Build only for linux-any: eventfd is used.
  * New policy version 3.9.5.

 -- YunQiang Su <wzssyqa@gmail.com>  Thu, 21 Nov 2013 11:11:56 +0800

sheepdog (0.7.3-1) unstable; urgency=low

  * Imported Upstream version 0.7.3
    The upstream has also the debian/* so merged with them.
  * Remove patches merged upstream
  * Don't source /lib/init/vars.sh any more
  * Use canonical vcs url and update homepage

 -- YunQiang Su <wzssyqa@gmail.com>  Sun, 22 Sep 2013 15:32:01 +0800

sheepdog (0.6.0-2~exp1) experimental; urgency=low

  * Fix another amd64 only assembly in include/logger.h.
  * Fix some gcc warning about alignment.

 -- YunQiang Su <wzssyqa@gmail.com>  Thu, 25 Jul 2013 22:56:30 +0800

sheepdog (0.6.0-2~exp) experimental; urgency=low

  * In lib/logger.c it use assembly only for amd64.

 -- YunQiang Su <wzssyqa@gmail.com>  Tue, 23 Jul 2013 16:31:16 +0800

sheepdog (0.6.0-1) unstable; urgency=low

  * Imported Upstream version 0.6.0
  * Don't source /lib/init/vars.sh in init

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 19 Jul 2013 23:13:49 +0800

sheepdog (0.5.6-2) unstable; urgency=low

  * Enable sheepfs support: build-dep on libfuse-dev.

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 19 Apr 2013 13:12:43 +0800

sheepdog (0.5.6-1) unstable; urgency=low

  * Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project. Closes: #694518
  * [Debconf translation updates]
  * Czech (Michal Simunek).  Closes: #696391
  * French (Julien Patriarca).  Closes: #696801
  * Portuguese (Pedro Ribeiro).  Closes: #696982
  * German (Chris Leick).  Closes: #697091
  * Spanish; (Camaleón).  Closes: #697106
  * Japanese (victory).  Closes: #697114
  * Russian (Yuri Kozlov).  Closes: #697179
  * Italian (Beatrice Torracca).  Closes: #697214
  * Galician (Jorge Barreiro).  Closes: #697228
  * Polish (Michał Kułach).  Closes: #697238
  * Swedish (Martin Bagge / brother).  Closes: #697293
  * Danish (Joe Hansen).  Closes: #697295

  [ Thomas Goirand <zigo@debian.org> ]
  * debian/copyright is now in machine readable format 1.0.
  * Manages /etc/default/sheepdog in a policy compliant way (eg: the file is
  copied from /usr/share/sheepdog in postinst, and isn't a conffiles).
  * Removes Guido Guenther <agx@debian.org> from uploaders as requested.
  * Uses debhelper 9 and compat 9, so there's hardening build flags.
  * More permisive sed call in postinst (eg: allows spaces and tabs before the
  variable).
  * Corrects debian/rules get-vcs-source to work like with the rest of the
  openstack packages.
  * Using xz as compression.
  * Added upstream changelog (out of a "git log" output).

  [ YunQiang Su <wzssyqa@gmail.com> ]
  * Import the new upstream tag 0.5.6 (Closes: #671466).
  * Create master branch act as upstream branch.
  * Add Simplified Chinese (zh_CN) translation for debconf.
  * Use pidfile to ensure service stop, remove the former hack.

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 08 Feb 2013 11:04:58 +0800

sheepdog (0.5.4-1) unstable; urgency=low

  * Add debian/watch; remove debian/gbp.conf
  * Imported Upstream version 0.5.4
  * Recommends corosync but not dependends now
  * Remove 0001-Skip-the-tests.patch and 0002-Add-collie-manpage.patch:
    in upstream tarball now
  * Use dh_autoreconf instead of running autogen.sh in rules
  * Add liburcu-dev to build depends
  * Add debconf and zookeeper support
  * Start service default
  * Bump to stand version 3.9.4; Priority to optional
  * Set PKG OpenStack <openstack-devel@lists.alioth.debian.org> as maintainer
    Set YunQiang Su <wzssyqa@gmail.com> and Guido Guenther <agx@debian.org>
      as Uploaders.

 -- YunQiang Su <wzssyqa@gmail.com>  Wed, 24 Oct 2012 11:01:24 +0800

sheepdog (0.3.0-3) unstable; urgency=low

  * [907284e] Make sheepdog linux-any since configure bails out on non linux
    architectures.
  * [c87c560] Add collie manpage. Thanks to Jens WEBER

 -- Guido Günther <agx@sigxcpu.org>  Wed, 04 Jul 2012 22:47:41 +0200

sheepdog (0.3.0-2) unstable; urgency=low

  * Upload to unstable
  * [eb4b22d] Use override instead of --before and --after to simplify the
    build logic and avoid the deprecation warning

 -- Guido Günther <agx@sigxcpu.org>  Wed, 04 Jul 2012 14:54:21 +0200

sheepdog (0.3.0-1) experimental; urgency=low

  [ Guido Günther ]
  * [9d6c032] Add gbp.conf
  * [3cf6e61] Bump Standards version
  * [1a4ec06] Add missing $remote_fs dependency
  * [450ea59] Install collie bash completion

  [ Jens Weber ]
  * [7d300a2] Add init script
  * [7ac0976] Skip the tests for now since they don't run from within pbuilder

 -- Guido Günther <agx@sigxcpu.org>  Sun, 24 Jun 2012 21:41:01 +0200

sheepdog (0.2.4-1) experimental; urgency=low

  * New upstream version
  * [e0f4d8a] Don't install init.d script

 -- Guido Günther <agx@sigxcpu.org>  Mon, 14 Nov 2011 15:35:17 +0100

sheepdog (0.2.0~0.git452bc6-1) unstable; urgency=low

  * Initial release (Closes: #606134)

 -- Guido Günther <agx@sigxcpu.org>  Tue, 07 Dec 2010 11:06:30 +0100
